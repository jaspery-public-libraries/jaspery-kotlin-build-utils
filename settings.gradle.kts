/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

rootProject.name = "jaspery-kotlin-build-utils"

include("jaspery-kotlin-mp-build")
include("jaspery-gitlab-utils")
include("jpl-build-env")

pluginManagement {
    @Suppress("UnstableApiUsage")
    plugins {
        val kotlinVersion: String by settings
        val testSetsPluginVersion: String by settings

        id("org.jetbrains.kotlin.jvm") version kotlinVersion
        id("org.unbroken-dome.test-sets") version testSetsPluginVersion
    }
}
