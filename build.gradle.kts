/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

plugins {
    kotlin("jvm")
    id("org.unbroken-dome.test-sets") apply false
    `maven-publish`
}

group = "com.jaspery.jaspery-public-libraries"
version = "1.0.0"

allprojects {
    apply { from("$rootDir/_repositories.gradle.kts") }
}

subprojects {
    group = "${rootProject.group}.${rootProject.name}"
    version = rootProject.version

    apply { plugin("org.jetbrains.kotlin.jvm") }
    apply { plugin("maven-publish") }

    // dependency management
    dependencies {
        val jplDepsPlatformVersion: String by project
        api(platform("com.jaspery.jaspery-public-libraries:jpl-dependencies-platform:$jplDepsPlatformVersion"))

        @Suppress("UnstableApiUsage")
        constraints {
            val gitlab4jVersion: String by project
            implementation("org.gitlab4j:gitlab4j-api:$gitlab4jVersion")
        }
    }

    publishing {
        publications {
            create<MavenPublication>("artifacts") {
                if (project.hasProperty("publishToMavenRepo")) {
                    val buildBranch: String? by project
                    val buildNumber: String by project
                    val defaultBranch: String by project

                    val versionBranchPart = buildBranch?.takeUnless { it == defaultBranch }?.let { "-$it" } ?: ""
                    val versionBuildNumberPart = buildNumber.takeUnless { it.isEmpty() }
                            ?.let { "-${it.padStart(6, '0')}" } ?: ""

                    version = "${project.version}$versionBuildNumberPart$versionBranchPart"
                }

                from(components["java"])
            }
        }

        repositories {
            if (project.hasProperty("publishToMavenRepo")) {
                val publishMavenRepoUrl: String by project
                val credentialsTokenType: String by project
                val credentialsTokenValue: String by project

                maven(publishMavenRepoUrl) {
                    name = "GitLab-${project.name}"
                    credentials(HttpHeaderCredentials::class) {
                        name = credentialsTokenType
                        value = credentialsTokenValue
                    }
                    authentication {
                        create<HttpHeaderAuthentication>("header")
                    }
                }
            }
        }
    }
}
