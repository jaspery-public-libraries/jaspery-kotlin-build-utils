/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */
package com.jaspery.gradle.ext.jacoco

import org.gradle.api.Task
import org.gradle.api.file.FileCollection
import org.gradle.api.provider.Provider
import org.gradle.kotlin.dsl.existing
import org.gradle.kotlin.dsl.getValue
import org.gradle.kotlin.dsl.provideDelegate
import org.gradle.testing.jacoco.tasks.JacocoReport
import org.jetbrains.kotlin.gradle.dsl.KotlinMultiplatformExtension
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet
import java.util.concurrent.Callable

open class JacocoKotlinMppJvmTestReport : JacocoReport() {
    init {
        val compileKotlinJvm by project.tasks.existing
        val jvmTest by project.tasks.existing
        val jvmMain by findKotlinMppExt().sourceSets.existing

        executionData(jvmTest.get())
        kotlinSourceSet(jvmMain)
        classDirectories.from(compileKotlinJvm)
        reports.xml.isEnabled = true
    }

    @Suppress("RedundantOverride")
    final override fun executionData(vararg tasks: Task?) {
        super.executionData(*tasks)
    }

    private fun findKotlinMppExt(): KotlinMultiplatformExtension {
        return project.extensions.getByName(kotlinExtName) as? KotlinMultiplatformExtension
                ?: throw IllegalStateException(kotlinMppExtNotFoundMsg)
    }

    private fun kotlinSourceSet(kotlinSourceSet: Provider<KotlinSourceSet>) {
        sourceDirectories.from(Callable { kotlinSourceSet.get().kotlin.sourceDirectories })
        additionalSourceDirs.from(object : Callable<FileCollection> {
            override fun call(): FileCollection {
                val kss = kotlinSourceSet.get()
                val result = kss.kotlin.sourceDirectories
                return kss.dependsOn.fold(result) { r, ss ->
                    r.plus(ss.kotlin.sourceDirectories)
                }
            }
        })
    }

    companion object {
        const val kotlinExtName = "kotlin"
        const val kotlinMppExtNotFoundMsg = "JacocoJvmTestReport test only supports MPP kotlin projects for now"
    }
}
