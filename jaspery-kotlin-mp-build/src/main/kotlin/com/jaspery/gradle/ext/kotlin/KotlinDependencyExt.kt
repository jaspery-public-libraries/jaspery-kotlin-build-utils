/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */
package com.jaspery.gradle.ext.kotlin

import org.gradle.kotlin.dsl.kotlin
import org.gradle.plugin.use.PluginDependenciesSpec
import org.gradle.plugin.use.PluginDependencySpec

private const val kotlinPluginPrefix = "org.jetbrains.kotlin."
private const val kotlinMultiplatformPluginModule = "multiplatform"

const val `kotlin-multiplatform` = "$kotlinPluginPrefix$kotlinMultiplatformPluginModule"

val PluginDependenciesSpec.`kotlin-multiplatform`: PluginDependencySpec
    get() = kotlin("kotlinMultiplatformPluginModule")
