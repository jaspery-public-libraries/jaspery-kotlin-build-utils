/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gradle.ext.kotlin

import org.gradle.api.artifacts.repositories.MavenRepositoryContentDescriptor

/**
 * Declares that all groups which belong to **`org.jetbrains`** should be searched
 * for in this repository.
 *
 * Regex of the group names: **`org\.jetbrains.*`**.
 */
fun MavenRepositoryContentDescriptor.approvedContentJetBrainsGroup() {
    includeGroupByRegex("org\\.jetbrains.*")
}
