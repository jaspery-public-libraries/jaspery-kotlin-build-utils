/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gradle.ext.jacoco

const val jacoco: String = "jacoco"
