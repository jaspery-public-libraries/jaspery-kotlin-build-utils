/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */
package com.jaspery.gradle.ext.kotlin

import org.gradle.api.Action
import org.jetbrains.kotlin.gradle.dsl.KotlinMultiplatformExtension
import org.jetbrains.kotlin.gradle.plugin.KotlinDependencyHandler
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet

fun KotlinMultiplatformExtension.commonMain(configure: Action<KotlinSourceSet>) =
        this.sourceSets.named("commonMain", configure)

fun KotlinMultiplatformExtension.commonTest(configure: Action<KotlinSourceSet>) =
        this.sourceSets.named("commonTest", configure)

fun KotlinMultiplatformExtension.jvmMain(configure: Action<KotlinSourceSet>) =
        this.sourceSets.named("jvmMain", configure)

fun KotlinMultiplatformExtension.jvmTest(configure: Action<KotlinSourceSet>) =
        this.sourceSets.named("jvmTest", configure)

fun KotlinMultiplatformExtension.jsMain(configure: Action<KotlinSourceSet>) =
        this.sourceSets.named("jsMain", configure)

fun KotlinMultiplatformExtension.jsTest(configure: Action<KotlinSourceSet>) =
        this.sourceSets.named("jsTest", configure)

fun KotlinMultiplatformExtension.commonMainDependencies(configure: Action<KotlinDependencyHandler>) =
        commonMain {
            dependencies {
                configure.execute(this)
            }
        }

fun KotlinMultiplatformExtension.commonTestDependencies(configure: Action<KotlinDependencyHandler>) =
        commonTest {
            dependencies {
                configure.execute(this)
            }
        }

fun KotlinMultiplatformExtension.jvmMainDependencies(configure: Action<KotlinDependencyHandler>) =
        jvmMain {
            dependencies {
                configure.execute(this)
            }
        }

fun KotlinMultiplatformExtension.jvmTestDependencies(configure: Action<KotlinDependencyHandler>) =
        jvmTest {
            dependencies {
                configure.execute(this)
            }
        }

fun KotlinMultiplatformExtension.jsMainDependencies(configure: Action<KotlinDependencyHandler>) =
        jsMain {
            dependencies {
                configure.execute(this)
            }
        }

fun KotlinMultiplatformExtension.jsTestDependencies(configure: Action<KotlinDependencyHandler>) =
        jsTest {
            dependencies {
                configure.execute(this)
            }
        }
