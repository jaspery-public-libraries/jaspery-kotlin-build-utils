/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

// There will be a lot of "unused" receiver parameters in this file
@file:Suppress("unused")

package com.jaspery.gradle.ext.dependencies

import org.gradle.kotlin.dsl.DependencyConstraintHandlerScope as DCHS
import org.gradle.kotlin.dsl.DependencyHandlerScope as DHS
import org.jetbrains.kotlin.gradle.plugin.KotlinDependencyHandler as KDH

/*
 * AssertK dependency notation helpers
 */
private const val assertkGroup = "com.willowtreeapps.assertk"

private fun assertk0(module: String? = null, version: String? = null): String =
        dependencyNotationStd(assertkGroup, "assertk", module, version)

/**
 * Creates the dependency notation for the named AssertK [module] at the given [version].
 *
 * If [module] is null or empty, it creates notation which describes "root" dependency
 * `com.willowtreeapps.assertk:assertk` it's useful for depending on multi-platform
 * libraries which publish gradle metadata descriptor.
 *
 * @receiver function only available in the context of [KDH]
 * @param module simple name of the AssertK module, for example "jvm".
 * @param version optional desired version, unspecified if null.
 * @return dependency notation "com.willowtreeapps.assertk:assertk(-<[module]]>(:<[version]>)"
 */
fun KDH.assertk(module: String? = null, version: String? = null): String = assertk0(module, version)

/**
 * Creates the dependency notation for the named AssertK [module] at the given [version].
 *
 * Same as [KDH.assertk] but available in [DCHS] context.
 *
 * @receiver function only available in the context of [DCHS]
 * @return dependency notation "com.willowtreeapps.assertk:assertk(-<[module]]>(:<[version]>)"
 */
fun DCHS.assertk(module: String? = null, version: String? = null): String = assertk0(module, version)

/*
 * JUnit notation helpers
 */
private fun junit0(version: String? = null): String =
        dependencyNotationStd("junit", "junit", null, version)

/**
 * Creates the dependency notation for the junit dependency of the given [version].
 *
 * @receiver function only available in the context of [KDH]
 * @param version optional desired version, unspecified if null.
 * @return dependency notation "junit:junit(:<[version]>).
 */
fun KDH.junit(version: String? = null): String = junit0(version)

/**
 * Creates the dependency notation for the junit dependency of the given [version].
 *
 * @receiver function only available in the context of [DCHS]
 * @param version optional desired version, unspecified if null.
 * @return dependency notation "junit:junit(:<[version]>).
 */
fun DCHS.junit(version: String? = null): String = junit0(version)

/*
 * TestNG notation helpers
 */
private fun testng0(version: String? = null): String =
        dependencyNotationStd("org.testng", "testng", null, version)

/**
 * Creates the dependency notation for the testng dependency of the given [version].
 *
 * @receiver function only available in the context of [KDH]
 * @param version optional desired version, unspecified if null.
 * @return dependency notation "org.testng:testng(:<[version]>).
 */
fun KDH.testng(version: String? = null): String = testng0(version)

/**
 * Creates the dependency notation for the testng dependency of the given [version].
 *
 * @receiver function only available in the context of [DCHS]
 * @param version optional desired version, unspecified if null.
 * @return dependency notation "org.testng:testng(:<[version]>).
 */
fun DCHS.testng(version: String? = null): String = testng0(version)

/**
 * Creates the dependency notation for the testng dependency of the given [version].
 *
 * @receiver function only available in the context of [DHS]
 * @param version optional desired version, unspecified if null.
 * @return dependency notation "org.testng:testng(:<[version]>).
 */
fun DHS.testng(version: String? = null): String = testng0(version)

/*
 * ThreeTenBP notation helpers
 */
private fun threeTenBp0(version: String? = null): String =
        dependencyNotationStd("org.threeten", "threetenbp", null, version)

/**
 * Creates the dependency notation for the threeTenBp dependency of the given [version].
 *
 * @receiver function only available in the context of [KDH]
 * @param version optional desired version, unspecified if null.
 * @return dependency notation "org.threeten:threetenbp(:<[version]>).
 */
fun KDH.threeTenBp(version: String? = null): String = threeTenBp0(version)

/**
 * Creates the dependency notation for the threeTenBp dependency of the given [version].
 *
 * @receiver function only available in the context of [DCHS]
 * @param version optional desired version, unspecified if null.
 * @return dependency notation "org.threeten:threetenbp(:<[version]>).
 */
fun DCHS.threeTenBp(version: String? = null): String = threeTenBp0(version)

/*
 * Jaspery Public Libraries notation helpers
 */
private const val jplGroupPrefix = "com.gitlab.jaspery-public-libraries"

private fun jpl0(groupSuffix: String, simpleModuleName: String, version: String? = null): String =
        dependencyNotationJitPackDependencies(jplGroupPrefix, groupSuffix, simpleModuleName, version)

/**
 * Creates the dependency notation for the one of Jaspery Public Library (JPL) of the given [version]
 * following the [JitPack's](htpps://jitpack.io) dependencies naming convention.
 *
 * @receiver function only available in the context of [KDH]
 * @param groupSuffix usually it's repository name which according to JitPack's conventions
 * is appended to dependency group name
 * @param simpleModuleName module name inside repository specified as [groupSuffix]
 * @param version optional desired version, unspecified if null.
 * @return dependency notation "com.gitlab.jaspery-public-libraries.<[groupSuffix]]>:<[simpleModuleName]>(:<[version]>)"
 */
fun KDH.jpl(groupSuffix: String, simpleModuleName: String, version: String? = null): String =
        jpl0(groupSuffix, simpleModuleName, version)

/**
 * Creates the dependency notation for the one of Jaspery Public Library (JPL) of the given [version]
 * following the [JitPack's](htpps://jitpack.io) dependencies naming convention.
 *
 * @see [KDH.jpl]
 *
 * @receiver function only available in the context of [DCHS]
 * @return dependency notation "com.gitlab.jaspery-public-libraries.<[groupSuffix]]>:<[simpleModuleName]>(:<[version]>)"
 */
fun DCHS.jpl(groupSuffix: String, simpleModuleName: String, version: String? = null): String =
        jpl0(groupSuffix, simpleModuleName, version)

/**
 * Creates the dependency notation for the one of Jaspery Public Library (JPL) of the given [version]
 * following the [JitPack's](htpps://jitpack.io) dependencies naming convention.
 *
 * @see [KDH.jpl]
 *
 * @receiver function only available in the context of [DHS]
 * @return dependency notation "com.gitlab.jaspery-public-libraries.<[groupSuffix]]>:<[simpleModuleName]>(:<[version]>)"
 */
fun DHS.jpl(groupSuffix: String, simpleModuleName: String, version: String? = null): String =
        jpl0(groupSuffix, simpleModuleName, version)

/**
 * Creates dependency notation in form of
 * `<[group]>:<[modulePrefix]>-<[moduleSuffix]:<[version]>>`,
 * [moduleSuffix] and [version] are optional.
 *
 * Convenient when specifying any standard gradle dependency notation. If [moduleSuffix] is `null` or empty
 * it results in `<[group]>:<[modulePrefix]>` which is especially convenient for multi-platform dependencies
 * when one need just specify dependency which publishes gradle metadata to common sourceSet and others are
 * resolved automatically for platform specific sourceSets, e.g.
 *
 * `dependencyNotationStd("com.willowtreeapps.assertk", "assertk", null, "0.19")` added to common
 * source set will be resolved to e.g. "assertk-jvm" and "assertk-js" for respective source sets.
 *
 * @param group full dependency group
 * @param modulePrefix dependency module prefix
 * @param moduleSuffix dependency module suffix
 * @param version dependency version
 *
 * @return dependency notation in form of `<[group]>:<[modulePrefix]>-<[moduleSuffix]:<[version]>>`
 */
private fun dependencyNotationStd(group: String, modulePrefix: String, moduleSuffix: String? = null, version: String? = null): String =
        "$group:$modulePrefix" +
                (if (moduleSuffix.isNullOrEmpty()) "" else "-$moduleSuffix") +
                (if (version.isNullOrEmpty()) "" else ":$version")

/**
 * Creates dependency notation in form of
 * `<[groupPrefix]>.<[groupSuffix]>-<[simpleModuleName]:<[version]>>`,
 * [simpleModuleName] and [version] are optional.
 *
 * Convenient when specifying gradle dependency for libraries published to [JitPack](https://jitpack.io) or similar.
 *
 * @param groupPrefix dependency group prefix
 * @param groupSuffix dependency group suffix
 * @param simpleModuleName simple dependency module name (without group)
 * @param version dependency version
 *
 * @return dependency notation in form of `<[groupPrefix]>.<[groupSuffix]>-<[simpleModuleName]:<[version]>>`
 */
@Suppress("SameParameterValue")
private fun dependencyNotationJitPackDependencies(groupPrefix: String, groupSuffix: String, simpleModuleName: String, version: String? = null): String =
        "$groupPrefix.$groupSuffix" +
                (if (simpleModuleName.isEmpty()) "" else ":$simpleModuleName") +
                (if (version.isNullOrEmpty()) "" else ":$version")
