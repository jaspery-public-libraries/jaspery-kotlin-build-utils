/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

@file:Suppress("unused")

package com.jaspery.gradle.ext.dependencies

import org.gradle.api.Action
import org.gradle.api.artifacts.*
import org.gradle.api.artifacts.dsl.ArtifactHandler
import org.gradle.api.artifacts.dsl.DependencyConstraintHandler
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.kotlin.dsl.accessors.runtime.addDependencyTo
import org.gradle.kotlin.dsl.accessors.runtime.addExternalModuleDependencyTo
import org.gradle.kotlin.dsl.add

/**
 * Adds a dependency to the 'jvmTestImplementation' configuration.
 *
 * @param dependencyNotation notation for the dependency to be added.
 * @return The dependency.
 *
 * @see [DependencyHandler.add]
 */
fun DependencyHandler.jvmTestImplementation(dependencyNotation: Any): Dependency? =
        add("jvmTestImplementation", dependencyNotation)

/**
 * Adds a dependency to the 'jvmTestImplementation' configuration.
 *
 * @param dependencyNotation notation for the dependency to be added.
 * @param dependencyConfiguration expression to use to configure the dependency.
 * @return The dependency.
 *
 * @see [DependencyHandler.add]
 */
fun DependencyHandler.jvmTestImplementation(
        dependencyNotation: String,
        dependencyConfiguration: Action<ExternalModuleDependency>
): ExternalModuleDependency = addDependencyTo(
        this, "jvmTestImplementation", dependencyNotation, dependencyConfiguration
)

/**
 * Adds a dependency to the 'jvmTestImplementation' configuration.
 *
 * @param group the group of the module to be added as a dependency.
 * @param name the name of the module to be added as a dependency.
 * @param version the optional version of the module to be added as a dependency.
 * @param configuration the optional configuration of the module to be added as a dependency.
 * @param classifier the optional classifier of the module artifact to be added as a dependency.
 * @param ext the optional extension of the module artifact to be added as a dependency.
 * @param dependencyConfiguration expression to use to configure the dependency.
 * @return The dependency.
 *
 * @see [DependencyHandler.create]
 * @see [DependencyHandler.add]
 */
fun DependencyHandler.jvmTestImplementation(
        group: String,
        name: String,
        version: String? = null,
        configuration: String? = null,
        classifier: String? = null,
        ext: String? = null,
        dependencyConfiguration: Action<ExternalModuleDependency>? = null
): ExternalModuleDependency = addExternalModuleDependencyTo(
        this, "jvmTestImplementation", group, name, version, configuration, classifier, ext, dependencyConfiguration
)

/**
 * Adds a dependency to the 'jvmTestImplementation' configuration.
 *
 * @param dependency dependency to be added.
 * @param dependencyConfiguration expression to use to configure the dependency.
 * @return The dependency.
 *
 * @see [DependencyHandler.add]
 */
fun <T : ModuleDependency> DependencyHandler.jvmTestImplementation(
        dependency: T,
        dependencyConfiguration: T.() -> Unit
): T = add("jvmTestImplementation", dependency, dependencyConfiguration)

/**
 * Adds a dependency constraint to the 'jvmTestImplementation' configuration.
 *
 * @param constraintNotation the dependency constraint notation
 *
 * @return the added dependency constraint
 *
 * @see [DependencyConstraintHandler.add]
 */
fun DependencyConstraintHandler.jvmTestImplementation(constraintNotation: Any): DependencyConstraint? =
        add("jvmTestImplementation", constraintNotation)

/**
 * Adds a dependency constraint to the 'jvmTestImplementation' configuration.
 *
 * @param constraintNotation the dependency constraint notation
 * @param block the block to use to configure the dependency constraint
 *
 * @return the added dependency constraint
 *
 * @see [DependencyConstraintHandler.add]
 */
fun DependencyConstraintHandler.jvmTestImplementation(constraintNotation: Any, block: DependencyConstraint.() -> Unit): DependencyConstraint? =
        add("jvmTestImplementation", constraintNotation, block)

/**
 * Adds an artifact to the 'jvmTestImplementation' configuration.
 *
 * @param artifactNotation the group of the module to be added as a dependency.
 * @return The artifact.
 *
 * @see [ArtifactHandler.add]
 */
fun ArtifactHandler.jvmTestImplementation(artifactNotation: Any): PublishArtifact =
        add("jvmTestImplementation", artifactNotation)

/**
 * Adds an artifact to the 'jvmTestImplementation' configuration.
 *
 * @param artifactNotation the group of the module to be added as a dependency.
 * @param configureAction The action to execute to configure the artifact.
 * @return The artifact.
 *
 * @see [ArtifactHandler.add]
 */
fun ArtifactHandler.jvmTestImplementation(
        artifactNotation: Any,
        configureAction: ConfigurablePublishArtifact.() -> Unit): PublishArtifact =
        add("jvmTestImplementation", artifactNotation, configureAction)
