/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */
plugins {
    `kotlin-dsl-base`
}

dependencies {
    implementation(kotlin("gradle-plugin"))
}
