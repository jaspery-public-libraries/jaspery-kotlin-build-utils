/*
 * Copyright © 2021. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

import org.unbrokendome.gradle.plugins.testsets.dsl.testSets

plugins {
    kotlin("jvm")
    id("org.unbroken-dome.test-sets")
}

testSets {
    "integrationTest"()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.gitlab4j:gitlab4j-api")
    testImplementation(kotlin("test-testng") as String) {
        exclude("junit")
    }
    testImplementation(kotlin("reflect"))
    testImplementation("com.willowtreeapps.assertk:assertk-jvm")
    testImplementation("io.mockk:mockk")
    testImplementation("com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext:kotlin-testng-dataprovider")
    testImplementation("io.insert-koin:koin-core")
    testImplementation("io.insert-koin:koin-test") {
        exclude("junit")
    }
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

val integrationTest by tasks.existing(Test::class) {
    useTestNG()
}

val processIntegrationTestResources by tasks.existing(Copy::class) {
    from(".vault.local") {
        into("")
    }
}
