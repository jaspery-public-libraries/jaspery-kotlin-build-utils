/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.workflow

import assertk.assertThat
import assertk.assertions.hasClass
import assertk.assertions.isEqualTo
import assertk.assertions.isFailure
import assertk.assertions.isTrue
import com.jaspery.gitlab.api.facade.GitLabApiFacade
import io.mockk.*
import org.gitlab4j.api.models.Issue
import org.gitlab4j.api.models.MergeRequest
import org.gitlab4j.api.models.Project
import org.gitlab4j.api.models.RepositoryFile
import org.testng.annotations.AfterClass
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test

class GitLabWorkflowDslTest {
    private lateinit var workflow: GitLabFixTicketWorkflow

    private val gitLabApiFacade: GitLabApiFacade = mockk()

    private val project: Project = mockk {
        every { defaultBranch } returns "master"
    }

    private val issue: Issue = mockk()

    private val mergeRequest: MergeRequest = mockk {
        every { iid } returns 42
        every { sourceBranch } returns SourceBranch
    }

    private val repositoryFile: RepositoryFile = mockk {
        every { decodedContentAsString } returns "I like to run asdf"
    }

    @Test
    fun `test use DSL to build workflow`() {
        workflow = workflow {
            connection {
                hostUrl = "https://gitlab.com"
                privateToken = "asdf"
            }
            project {
                groupId = "jaspery-public-libraries"
                projectId = "jaspery-kotlin-ext"
            }
            issue {
                title = "Upgrade Gradle"
                description = "test"
            }
        }

        assertThat {
            workflow.mergeRequest
        }.isFailure().hasClass(IllegalStateException::class)
    }

    @Test(dependsOnMethods = ["test use DSL to build workflow"])
    fun `test create merge request and submit patch`() {
        every { gitLabApiFacade.getProject("jaspery-public-libraries/jaspery-kotlin-ext") } returns project
        every { gitLabApiFacade.createIssue(project, Title, "test") } returns issue
        every { gitLabApiFacade.createBranchAndMergeRequest(project, SourceBranch, "master", Title, "test") } returns mergeRequest
        every { gitLabApiFacade.getMergeRequest(project, 42) } returns mergeRequest
        every { gitLabApiFacade.getFile(project, "gradle/asdf", SourceBranch) } returns repositoryFile
        every { gitLabApiFacade.updateFile(project, repositoryFile, SourceBranch, "asdf") } returns repositoryFile

        val captureContent = CapturingSlot<String>()

        every { repositoryFile.encodeAndSetContent(capture(captureContent)) } just Runs

        workflow.createMergeRequest()
        workflow.submitPatch {
            commitMessage = "asdf"
            file {
                filePath = "gradle/asdf"
                regex = "asdf"
                replacement = "qwer"
            }
        }
        workflow.resolveMergeRequest()

        assertThat(captureContent.isCaptured).isTrue()
        assertThat(captureContent.captured).isEqualTo(ExpectedResult)

        verify(exactly = 1) { gitLabApiFacade.updateFile(project, repositoryFile, SourceBranch, "asdf") }
    }

    @BeforeClass
    fun mockHiddenDependencies() {
        mockkConstructor(GitLabApiFacade.Factory::class)
        every { anyConstructed<GitLabApiFacade.Factory>().newInstance() } returns gitLabApiFacade
    }

    @AfterClass
    fun unmockHiddenDependencies() {
        unmockkConstructor(GitLabApiFacade.Factory::class)
    }

    companion object {
        private const val ExpectedResult = "I like to run qwer"
        private const val SourceBranch = "Upgrade-Gradle"
        private const val Title = "Upgrade Gradle"
    }
}
