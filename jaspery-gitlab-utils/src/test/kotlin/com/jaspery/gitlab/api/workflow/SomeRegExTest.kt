/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.workflow

import assertk.assertThat
import assertk.assertions.isEqualTo
import org.testng.annotations.DataProvider
import org.testng.annotations.Test

class SomeRegExTest {
    @Test(dataProvider = "testCasesDataProvider")
    internal fun testRegExp(testCase: RegExTestCase) = with(testCase) {
        println(fileName)
        val regex: Regex = regExpStr.toRegex()
        val matchResult = regex.find(initialContent)
        val range = when (matchResult?.groups?.size) {
            null -> IntRange.EMPTY
            1 -> matchResult.range
            else -> matchResult.groups[1]?.range ?: IntRange.EMPTY
        }

        val result = initialContent.replaceRange(range, testCase.replacement)

        assertThat(result).isEqualTo(testCase.modifiedContent)
        println(result)
    }

    @DataProvider
    fun testCasesDataProvider(): Iterator<Any> = TestData.filter { it.regExpStr.isNotBlank() }.iterator()

    companion object {
        internal val TestData = listOf(
                RegExTestCase(
                        "README.md",
                        "does not have match",
                        "",
                        """
                            This is default README file for test repo
                            And this is new line
                        """.trimIndent(),
                        """
                            This is default README file for test repo
                            And this is new line
                        """.trimIndent()
                ),

                RegExTestCase(
                        "gradle-wrapper.properties",
                        "gradle-(5\\.6\\.3)-all.zip",
                        "6.0.1",
                        """
                            distributionBase=GRADLE_USER_HOME
                            distributionPath=wrapper/dists
                            distributionUrl=https\://services.gradle.org/distributions/gradle-5.6.3-all.zip
                            zipStoreBase=GRADLE_USER_HOME
                            zipStorePath=wrapper/dists
                        """.trimIndent(),
                        """
                            distributionBase=GRADLE_USER_HOME
                            distributionPath=wrapper/dists
                            distributionUrl=https\://services.gradle.org/distributions/gradle-6.0.1-all.zip
                            zipStoreBase=GRADLE_USER_HOME
                            zipStorePath=wrapper/dists
                        """.trimIndent()
                ),
                RegExTestCase(
                        "gradle.properties",
                        "kotlinVersion=1\\.3\\.50",
                        "kotlinVersion=1.3.61",
                        """
                            kotlinVersion=1.3.50
                            springDepMgtVersion=1.0.8.RELEASE
                        """.trimIndent(),
                        """
                            kotlinVersion=1.3.61
                            springDepMgtVersion=1.0.8.RELEASE
                        """.trimIndent()
                )
        )
    }

    internal data class RegExTestCase(
            val fileName: String,
            val regExpStr: String,
            val replacement: String,
            val initialContent: String,
            val modifiedContent: String
    )
}
