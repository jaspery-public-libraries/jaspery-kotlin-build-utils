/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.workflow

import assertk.all
import assertk.assertThat
import assertk.assertions.*
import assertk.fail
import com.jaspery.gitlab.api.workflow.WorkflowAction.*
import com.jaspery.gitlab.api.workflow.WorkflowState.*
import com.jaspery.kotlin.testng.dataprovider.dataProvider
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import kotlin.reflect.KClass

class WorkflowStateTest {
    @Test(dataProvider = "possibleStateNames")
    fun testAllPossibleStatesExist(possibleStateNames: List<String>) {
        val statesClasses = WorkflowState::class.sealedSubclasses
        val simpleNames = statesClasses.map { it.simpleName ?: fail("unexpected sealed class $it") }
        assertThat(simpleNames).all {
            isNotEmpty()
            hasSize(possibleStateNames.size)
            containsAll(*possibleStateNames.toTypedArray())
        }
    }

    @Test(dataProvider = "possibleTransitions")
    internal fun testTransitions(inState: WorkflowState, action: WorkflowAction, outState: KClass<WorkflowState>) {
        assertThat(inState.transition(action)).isEqualTo(outState)
    }

    @Test
    fun testConsumeAction() {
        val state: WorkflowState = Initialized
        val action: WorkflowAction = mockk<CreateMergeRequest>()

        every { action.execute(Initialized) } returns mockk<AcceptingPatch>()

        val result = state.consumeAction(action)

        assertThat(result).hasClass(AcceptingPatch::class)

        verify(exactly = 1) { action.execute(Initialized) }
    }

    @Test
    fun testConsumeActionIncorrectTransition() {
        val action: WorkflowAction = mockk<WorkflowAction>()

        assertThat {
            Initialized.consumeAction(action)
        }.all {
            isFailure().hasClass(IllegalStateException::class)
        }
    }

    @Test
    fun testConsumeActionIncorrectState() {
        val action: WorkflowAction = mockk<CreateMergeRequest>()

        every { action.execute(Initialized) } returns mockk<WorkflowState>()

        assertThat {
            Initialized.consumeAction(action)
        }.isFailure().hasClass(IllegalStateException::class)

        verify(exactly = 1) { action.execute(Initialized) }
    }

    @DataProvider
    fun possibleStateNames() = dataProvider {
        scenario(listOf(
                "AcceptedPatch",
                "AcceptingPatch",
                "Finished",
                "IncorrectTransition",
                "Initialized"
        ))
    }.testNGDataArray()

    @DataProvider
    fun possibleTransitions() = dataProvider {
        scenario(Initialized, mockk<CreateMergeRequest>(), AcceptingPatch::class)
        scenario(Initialized, mockk<ResolveMergeRequest>(), Finished::class)
        scenario(Initialized, mockk<WorkflowAction>(), IncorrectTransition::class)

        scenario(AcceptingPatch(MergeRequestModel(1)), mockk<SubmitPatch>(), AcceptedPatch::class)
        scenario(AcceptingPatch(MergeRequestModel(1)), mockk<ResolveMergeRequest>(), Finished::class)
        scenario(AcceptingPatch(MergeRequestModel(1)), mockk<WorkflowAction>(), IncorrectTransition::class)

        scenario(AcceptedPatch, mockk<ResolveMergeRequest>(), Finished::class)
        scenario(AcceptedPatch, mockk<WorkflowAction>(), IncorrectTransition::class)

        scenario(Finished, mockk<WorkflowAction>(), IncorrectTransition::class)
        scenario(IncorrectTransition, mockk<WorkflowAction>(), IncorrectTransition::class)
    }.testNGDataArray()
}
