/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.facade

import assertk.all
import assertk.assertThat
import assertk.assertions.hasClass
import assertk.assertions.hasMessage
import assertk.assertions.isEqualTo
import assertk.assertions.isFailure
import io.mockk.*
import org.gitlab4j.api.*
import org.gitlab4j.api.models.*
import org.testng.annotations.AfterMethod
import org.testng.annotations.Test
import java.util.*

class GitLabApiFacadeTest {
    // constants
    private val groupPath = "path"
    private val projectPath = "path"

    // API
    private val groupApi: GroupApi = mockk()
    private val projectApi: ProjectApi = mockk()
    private val issuesApi: IssuesApi = mockk()
    private val mergeRequestApi: MergeRequestApi = mockk()
    private val repositoryFileApi: RepositoryFileApi = mockk()
    private val repositoryApi: RepositoryApi = mockk()
    private val commitsApi: CommitsApi = mockk()

    // Entities
    private val group: Group = mockk()
    private val project: Project = mockk()
    private val issue: Issue = mockk()
    private val mergeRequest: MergeRequest = mockk()
    private val repositoryFile: RepositoryFile = mockk()
    private val branch: Branch = mockk()

    private val gitLabApiFacade: GitLabApiFacade = GitLabApiFacade(groupApi, projectApi, commitsApi, repositoryApi, repositoryFileApi, mergeRequestApi, issuesApi)

    @AfterMethod
    fun clearApiMocks() = clearMocks(groupApi, projectApi, issuesApi, mergeRequestApi, repositoryFileApi, repositoryApi, commitsApi)

    @AfterMethod
    fun clearEntityMocks() = clearMocks(group, project, issue, mergeRequest, repositoryFile, branch)

    @Test
    fun `test getProject`() {
        every { projectApi.getProject(projectPath) } returns project

        val result = gitLabApiFacade.getProject(projectPath)

        assertThat(result).isEqualTo(project)
        verify(exactly = 1) { projectApi.getProject(projectPath) }
    }

    @Test
    fun `test getProject does not exist`() {
        every { projectApi.getProject(projectPath) } returns null

        assertThat {
            gitLabApiFacade.getProject(projectPath)
        }.isFailure().hasClass(IllegalArgumentException::class)

        verify(exactly = 1) { projectApi.getProject(projectPath) }
    }

    @Test
    fun `test getProject illegal argument`() {
        assertThat {
            gitLabApiFacade.getProject(" ")
        }.isFailure().all {
            hasClass(IllegalArgumentException::class)
            hasMessage("Path should not be blank")
        }
    }

    @Test
    fun `test getOrCreateProject project exists`() {
        every { project.path } returns projectPath
        every { projectApi.getProject(projectPath) } returns project

        val result = gitLabApiFacade.getOrCreateProject(projectPath, project)

        assertThat(result).isEqualTo(project)
        verify(exactly = 1) { projectApi.getProject(projectPath) }
    }

    @Test
    fun `test getOrCreateProject project does not exist`() {
        every { project.path } returns projectPath
        every { projectApi.getProject(projectPath) } returns null
        every { projectApi.createProject(project) } returns project

        val result = gitLabApiFacade.getOrCreateProject(projectPath, project)

        assertThat(result).isEqualTo(project)
        verify(exactly = 1) { projectApi.getProject(projectPath) }
        verify(exactly = 1) { projectApi.createProject(project) }
    }

    @Test
    fun `test getOrCreateProject illegal argument`() {
        every { project.path } returns projectPath

        assertThat {
            gitLabApiFacade.getOrCreateProject(" ", project)
        }.isFailure().hasClass(IllegalArgumentException::class)
    }

    @Test
    fun `test getOrCreateGroup group exists`() {
        every { group.path } returns groupPath
        every { groupApi.getGroup(groupPath) } returns group

        val result = gitLabApiFacade.getOrCreateGroup(groupPath, group)

        assertThat(result).isEqualTo(group)
        verify(exactly = 1) { groupApi.getGroup(groupPath) }
    }

    @Test
    fun `test getOrCreateGroup group does not exist`() {
        every { group.path } returns groupPath
        every { groupApi.getGroup(groupPath) } returns null
        every { groupApi.addGroup(group) } returns group

        val result = gitLabApiFacade.getOrCreateGroup(groupPath, group)

        assertThat(result).isEqualTo(group)
        verify(exactly = 1) { groupApi.getGroup(groupPath) }
        verify(exactly = 1) { groupApi.addGroup(group) }
    }

    @Test
    fun `test getOrCreateGroup illegal argument`() {
        every { group.path } returns null

        assertThat {
            gitLabApiFacade.getOrCreateGroup(groupPath, group)
        }.isFailure().hasClass(IllegalArgumentException::class)

        assertThat {
            gitLabApiFacade.getOrCreateGroup(" ", group)
        }.isFailure().hasClass(IllegalArgumentException::class)
    }

    @Test
    fun `test create issue`() {
        every { issuesApi.createIssue(project, "title", "description") } returns issue

        val result = gitLabApiFacade.createIssue(project, "title", "description")

        assertThat(result).isEqualTo(issue)
        verify(exactly = 1) { issuesApi.createIssue(project, "title", "description") }
    }

    @Test
    fun `test getMergeRequest`() {
        every { mergeRequestApi.getMergeRequest(project, 42) } returns mergeRequest

        val result = gitLabApiFacade.getMergeRequest(project, 42)

        assertThat(result).isEqualTo(mergeRequest)
        verify(exactly = 1) { mergeRequestApi.getMergeRequest(project, 42) }
    }

    @Test
    fun `test getFile`() {
        every { repositoryFileApi.getFile(project, "file", "branch", true) } returns repositoryFile

        val result = gitLabApiFacade.getFile(project, "file", "branch")
        assertThat(result).isEqualTo(repositoryFile)

        verify(exactly = 1) { repositoryFileApi.getFile(project, "file", "branch", true) }
    }

    @Test
    fun `test updateFile`() {
        every { repositoryFileApi.updateFile(project, repositoryFile, "branch", "commit message") } returns repositoryFile

        val result = gitLabApiFacade.updateFile(project, repositoryFile, "branch", "commit message")
        assertThat(result).isEqualTo(repositoryFile)

        verify(exactly = 1) { repositoryFileApi.updateFile(project, repositoryFile, "branch", "commit message") }
    }

    @Test
    fun `test createBranchAndMergeRequest`() {
        val newBranch = "branch"
        val defaultBranch = "master"
        val title = "title"
        every { branch.name } returns newBranch
        every { repositoryApi.createBranch(project, newBranch, defaultBranch) } returns branch
        every { mergeRequestApi.createMergeRequest(project, newBranch, defaultBranch, title, any(), any()) } returns mergeRequest

        val result = gitLabApiFacade.createBranchAndMergeRequest(project, newBranch, defaultBranch, title, "")

        assertThat(result).isEqualTo(mergeRequest)
        verify(exactly = 1) { repositoryApi.createBranch(project, newBranch, defaultBranch) }
        verify(exactly = 1) { mergeRequestApi.createMergeRequest(project, newBranch, defaultBranch, title, any(), any()) }
    }

    @Test
    fun `test createMultipleFiles`() {
        val branchName = "master"
        val commitMessage = "commit message"
        val files = listOf(
                RepositoryFileContent("file1.txt", "I like to run"),
                RepositoryFileContent("file2.txt", "Out in the sun")
        )

        every { repositoryFileApi.getOptionalFile(project, "file1.txt", branchName) } returns Optional.of(repositoryFile)
        every { repositoryFileApi.getOptionalFile(project, "file2.txt", branchName) } returns Optional.empty()
        every { commitsApi.createCommit(project, any()) } returns mockk()

        gitLabApiFacade.createMultipleFiles(project, branchName, commitMessage, files)

        verify(exactly = 2) { repositoryFileApi.getOptionalFile(project, any(), branchName) }
        verify(exactly = 1) { commitsApi.createCommit(project, any()) }
    }

    @Test
    fun `test dropBranchAndMergeRequestIfOpen`() {
        val projectId = 42
        val mrIid = 146
        val mrTitle = "MR Title"
        val sourceBranch = "sourceBranch"

        every { project.id } returns projectId
        every { mergeRequest.title } returns mrTitle
        every { mergeRequest.iid } returns mrIid
        every { mergeRequest.sourceBranch } returns sourceBranch

        every { mergeRequestApi.getMergeRequests(any()) } returns listOf(mergeRequest)
        every { mergeRequestApi.deleteMergeRequest(project, mrIid) } just Runs
        every { repositoryApi.deleteBranch(project, sourceBranch) } just Runs

        gitLabApiFacade.dropBranchAndMergeRequestIfOpen(project, mrTitle)

        verify(exactly = 1) { mergeRequestApi.getMergeRequests(any()) }
        verify(exactly = 1) { mergeRequestApi.deleteMergeRequest(project, mrIid) }
        verify(exactly = 1) { repositoryApi.deleteBranch(project, sourceBranch) }
    }

    @Test
    fun `test dropIssueIfOpen`() {
        val issueTitle = "Issue Title"
        val issueIid = 42

        every { issue.title } returns issueTitle
        every { issue.iid } returns issueIid

        every { issuesApi.getIssues(project, any<IssueFilter>()) } returns listOf(issue)
        every { issuesApi.deleteIssue(project, issueIid) } just Runs

        gitLabApiFacade.dropIssueIfOpen(project, issueTitle)

        verify(exactly = 1) { issuesApi.getIssues(project, any<IssueFilter>()) }
        verify(exactly = 1) { issuesApi.deleteIssue(project, issueIid) }
    }
}
