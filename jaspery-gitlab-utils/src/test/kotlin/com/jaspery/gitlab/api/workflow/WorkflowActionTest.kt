/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.workflow

import assertk.assertThat
import assertk.assertions.hasClass
import assertk.assertions.isEqualTo
import assertk.assertions.isFailure
import assertk.assertions.isFalse
import com.jaspery.gitlab.api.facade.GitLabApiFacade
import com.jaspery.gitlab.api.workflow.GitLabFixTicketWorkflowModel.IssueModel
import com.jaspery.gitlab.api.workflow.GitLabFixTicketWorkflowModel.ProjectModel
import com.jaspery.gitlab.api.workflow.GitLabPatchModel.RepositoryFilePatchModel
import com.jaspery.kotlin.testng.dataprovider.dataProvider
import io.mockk.*
import org.gitlab4j.api.models.Issue
import org.gitlab4j.api.models.MergeRequest
import org.gitlab4j.api.models.Project
import org.gitlab4j.api.models.RepositoryFile
import org.testng.annotations.AfterMethod
import org.testng.annotations.DataProvider
import org.testng.annotations.Test

class WorkflowActionTest {
    companion object {
        const val GroupId = "group"
        const val ProjectId = "project"

        const val IssueTitle = "Issue Title"
        const val IssueDescription = "Issue Description"

        const val CommitMessage = "Commit Message"
        const val FileName = "gradle.properties"
        const val FileContent = "kotlinVersion=1.3.50"
        const val FileResult = "kotlinVersion=1.3.61"

        const val DefaultBranch = "master"
        const val MrIid = 146
    }

    // dependencies
    private val issueModel = IssueModel(IssueTitle, IssueDescription)
    private val projectModel = ProjectModel(GroupId, ProjectId)
    private val mergeRequestModel = MergeRequestModel(MrIid)
    private val gitLabApi: GitLabApiFacade = mockk()

    // dependencies behaviour
    private val project: Project = Project().apply {
        defaultBranch = DefaultBranch
    }
    private val issue: Issue = mockk()
    private val mergeRequest: MergeRequest = MergeRequest().apply {
        iid = MrIid
        sourceBranch = "\\s+".toRegex().replace(IssueTitle, "-")
    }

    // state
    private val currentState: WorkflowState = mockk()

    @Test
    fun testCreateMergeRequest() {
        every { gitLabApi.getProject("$GroupId/$ProjectId") } returns project
        every { gitLabApi.createIssue(project, IssueTitle, IssueDescription) } returns issue
        every {
            gitLabApi.createBranchAndMergeRequest(project, any(), DefaultBranch, IssueTitle, IssueDescription)
        } returns mergeRequest

        val action = WorkflowAction.CreateMergeRequest(gitLabApi, projectModel, issueModel)
        val result = action.execute(currentState)

        println(result)

        assertThat(result).hasClass(WorkflowState.AcceptingPatch::class)
        assertThat((result as WorkflowState.AcceptingPatch).mergeRequest.IID).isEqualTo(MrIid)

        verify(exactly = 1) { gitLabApi.getProject("$GroupId/$ProjectId") }
        verify(exactly = 1) { gitLabApi.createIssue(project, IssueTitle, IssueDescription) }
        verify(exactly = 1) {
            gitLabApi.createBranchAndMergeRequest(
                    project, match { !it.matches("\\s+".toRegex()) }, DefaultBranch, IssueTitle, IssueDescription)
        }
    }

    @Test(dataProvider = "submitPatchDataProvider")
    fun testSubmitPatch(testName: String, fileContent: String, resultFileContent: String, patchModel: GitLabPatchModel) {
        every { gitLabApi.getProject("$GroupId/$ProjectId") } returns project
        every { gitLabApi.getMergeRequest(project, MrIid) } returns mergeRequest

        val file: RepositoryFile = mockk()
        val content: CapturingSlot<String> = slot()

        every { gitLabApi.getFile(project, FileName, mergeRequest.sourceBranch) } returns file
        every { file.decodedContentAsString } returns fileContent
        every { file.encodeAndSetContent(capture(content)) } just Runs
        every { gitLabApi.updateFile(project, file, mergeRequest.sourceBranch, CommitMessage) } returns file

        val action = WorkflowAction.SubmitPatch(gitLabApi, projectModel, patchModel)
        val result = action.execute(WorkflowState.AcceptingPatch(mergeRequestModel))

        assertThat(result).isEqualTo(WorkflowState.AcceptedPatch)

        verify(exactly = 1) { gitLabApi.getProject("$GroupId/$ProjectId") }
        verify(exactly = 1) { gitLabApi.getMergeRequest(project, MrIid) }
        verify(exactly = 1) { gitLabApi.getFile(project, FileName, mergeRequest.sourceBranch) }
        verify(exactly = 1) { file.decodedContentAsString }

        if (fileContent != resultFileContent) {
            assertThat(content.captured).isEqualTo(resultFileContent)
            verify(exactly = 1) { file.encodeAndSetContent(capture(content)) }
            verify(exactly = 1) { gitLabApi.updateFile(project, file, mergeRequest.sourceBranch, CommitMessage) }
        } else {
            assertThat(content.isCaptured).isFalse()
        }
    }

    @DataProvider
    fun submitPatchDataProvider() = dataProvider {
        scenario("capt grp regex", FileContent, FileResult, newPatchModel("kotlinVersion=(1\\.3\\.50)", "1.3.61"))
        scenario("standard regex", FileContent, FileResult, newPatchModel("kotlinVersion=1.3.50", "kotlinVersion=1.3.61"))
        scenario("no match regex", FileContent, FileContent, newPatchModel("kotlinVersion=1.3.51", "kotlinVersion=1.3.61"))
    }.testNGDataArray()

    private fun newPatchModel(regex: String, replacement: String) =
            GitLabPatchModel(CommitMessage, RepositoryFilePatchModel(FileName, regex, replacement))

    @Test
    fun testSubmitPatchIllegalState() {
        val action = WorkflowAction.SubmitPatch(gitLabApi, projectModel, mockk())

        assertThat {
            action.execute(currentState)
        }.isFailure().hasClass(IllegalStateException::class)
    }

    @Test
    fun testResolveMergeRequest() {
        val action = WorkflowAction.ResolveMergeRequest()
        val result = action.execute(currentState)
        assertThat(result).isEqualTo(WorkflowState.Finished)
    }

    @AfterMethod
    fun clearMocks() {
        clearMocks(gitLabApi)
    }
}