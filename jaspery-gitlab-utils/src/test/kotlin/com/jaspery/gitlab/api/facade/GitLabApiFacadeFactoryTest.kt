/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.facade

import org.testng.annotations.Test

class GitLabApiFacadeFactoryTest {
    @Test
    fun testFactory() {
        val host = "http://host"
        val token = "token"
        val connection = GitLabConnection(host, token)
        val factory = GitLabApiFacade.Factory(connection)

        val gitLabApiFacade = factory.newInstance()
    }
}