/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.facade

import assertk.all
import assertk.assertThat
import assertk.assertions.*
import org.testng.annotations.Test
import java.net.MalformedURLException

class GitLabConnectionTest {
    @Test
    fun testGitLabConnectionConstructor() {
        GitLabConnection(GITLAB_SERVER, GITLAB_PRIVATE_TOKEN)
    }

    @Test
    fun testGitLabConnectionConstructorInvalidUrl() {
        assertThat {
            GitLabConnection("1" + GITLAB_SERVER, GITLAB_PRIVATE_TOKEN)
        }.isFailure().all {
            hasClass(IllegalArgumentException::class)
            cause().isNotNull().hasClass(MalformedURLException::class)
            messageContains("Invalid url")
        }
    }

    companion object {
        const val GITLAB_SERVER = "https://gitlab.com"
        const val GITLAB_PRIVATE_TOKEN = "adsf"
    }
}