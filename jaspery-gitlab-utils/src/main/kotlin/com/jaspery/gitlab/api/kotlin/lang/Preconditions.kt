package com.jaspery.gitlab.api.kotlin.lang

internal inline fun <V : CharSequence> requireNotBlank(value: V, lazyMessage: () -> Any): V {
    return require(value, { it.isNotBlank() }, lazyMessage)
}

internal fun <V : CharSequence> requireNotBlank(value: V): V {
    return require(value, { it.isNotBlank() })
}

/**
 * Throws an [IllegalArgumentException] with the result of calling [lazyMessage]
 * if the [value] does not match [predicate]'s condition. Otherwise returns [value].
 */
internal inline fun <T> require(value: T, predicate: (T) -> Boolean, lazyMessage: () -> Any): T {
    require(predicate(value), lazyMessage)
    return value
}

/**
 * Throws an [IllegalArgumentException] if the [value] does not match [predicate]'s
 * condition. Otherwise returns [value].
 */
internal inline fun <T> require(value: T, predicate: (T) -> Boolean): T {
    require(predicate(value))
    return value
}
