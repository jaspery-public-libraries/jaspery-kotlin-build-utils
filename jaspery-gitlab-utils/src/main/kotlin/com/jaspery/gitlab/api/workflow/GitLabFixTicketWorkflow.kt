/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.workflow

interface GitLabFixTicketWorkflow {
    val mergeRequest: MergeRequestModel

    fun createMergeRequest()
    fun submitPatch(init: GitLabPatchModelBuilder.() -> Unit)
    fun resolveMergeRequest()
}