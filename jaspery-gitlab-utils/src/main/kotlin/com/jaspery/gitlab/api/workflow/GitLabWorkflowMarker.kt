/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.workflow

@DslMarker
annotation class GitLabWorkflowMarker
