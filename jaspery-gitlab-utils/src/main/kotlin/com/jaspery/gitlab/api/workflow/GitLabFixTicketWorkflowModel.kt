/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.workflow

data class GitLabFixTicketWorkflowModel(
        val projectModel: ProjectModel,
        val issueModel: IssueModel
) {
    data class ProjectModel(val groupId: String, val projectId: String) {
        val projectPath = if (groupId.isBlank()) projectId else "$groupId/$projectId"
    }

    data class IssueModel(val title: String, val description: String)
}
