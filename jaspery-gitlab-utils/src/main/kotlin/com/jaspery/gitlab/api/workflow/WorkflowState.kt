/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.workflow

import com.jaspery.gitlab.api.workflow.WorkflowAction.*
import kotlin.reflect.KClass

internal sealed class WorkflowState {
    internal fun transition(action: WorkflowAction): KClass<out WorkflowState> = Transitions.transition(this, action)

    object Initialized : WorkflowState()
    class AcceptingPatch(val mergeRequest: MergeRequestModel) : WorkflowState()
    object AcceptedPatch : WorkflowState()
    object Finished : WorkflowState()
    object IncorrectTransition : WorkflowState()

    private object Transitions {
        fun transition(state: WorkflowState, action: WorkflowAction): KClass<out WorkflowState> =
                when (state) {
                    is Initialized -> when (action) {
                        is CreateMergeRequest -> AcceptingPatch::class
                        is ResolveMergeRequest -> Finished::class
                        else -> IncorrectTransition::class
                    }
                    is AcceptingPatch -> when (action) {
                        is SubmitPatch -> AcceptedPatch::class
                        is ResolveMergeRequest -> Finished::class
                        else -> IncorrectTransition::class
                    }
                    is AcceptedPatch -> when (action) {
                        is ResolveMergeRequest -> Finished::class
                        else -> IncorrectTransition::class
                    }
                    is Finished -> IncorrectTransition::class
                    is IncorrectTransition -> IncorrectTransition::class
                }
    }

    internal fun consumeAction(action: WorkflowAction): WorkflowState {
        val nextStateClass = transition(action)
        if (nextStateClass == IncorrectTransition::class) {
            throw IllegalStateException("Incorrect transition from state $this via action $action")
        }
        val nextState = action.execute(this)
        if (!nextStateClass.isInstance(nextState)) {
            throw IllegalStateException("Incorrect state $this, $nextStateClass expected")
        }
        return nextState
    }
}
