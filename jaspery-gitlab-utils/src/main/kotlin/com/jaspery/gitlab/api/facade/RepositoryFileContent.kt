/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.facade

data class RepositoryFileContent(val path: String, val content: String)