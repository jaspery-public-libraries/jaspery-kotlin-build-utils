/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.facade

import java.net.MalformedURLException
import java.net.URL

typealias PrivateToken = String
typealias HostUrl = String

class GitLabConnection(hostUrl: HostUrl, val privateToken: PrivateToken) {
    val hostUrl = checkUrl(hostUrl)

    private fun checkUrl(hostUrl: HostUrl): HostUrl = try {
        hostUrl.also { URL(it) }
    } catch (e: MalformedURLException) {
        throw IllegalArgumentException("Invalid url: $hostUrl", e)
    }
}
