/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.workflow

import com.jaspery.gitlab.api.facade.GitLabApiFacade
import com.jaspery.gitlab.api.workflow.GitLabFixTicketWorkflowModel.IssueModel
import com.jaspery.gitlab.api.workflow.GitLabFixTicketWorkflowModel.ProjectModel
import com.jaspery.gitlab.api.workflow.WorkflowState.*

internal sealed class WorkflowAction {
    abstract fun execute(currentState: WorkflowState): WorkflowState

    class CreateMergeRequest(
            private val gitLabApi: GitLabApiFacade,
            private val projectModel: ProjectModel,
            private val i: IssueModel
    ) : WorkflowAction() {
        override fun execute(currentState: WorkflowState): WorkflowState {
            val project = gitLabApi.getProject(projectModel.projectPath)
            val defaultBranchName = project.defaultBranch
            val newBranchName = genBranchName(i)

            gitLabApi.createIssue(project, i.title, i.description)
            val mergeRequest = gitLabApi.createBranchAndMergeRequest(project, newBranchName, defaultBranchName, i.title, i.description)
            return AcceptingPatch(MergeRequestModel(mergeRequest.iid))
        }

        private fun genBranchName(i: IssueModel) = i.title.replace(Regex("\\s+"), "-")
    }

    class SubmitPatch(
            private val gitLabApi: GitLabApiFacade,
            private val projectModel: ProjectModel,
            private val patchModel: GitLabPatchModel
    ) : WorkflowAction() {
        override fun execute(currentState: WorkflowState): WorkflowState {
            if (currentState !is AcceptingPatch) {
                throw IllegalStateException("Illegal state: $currentState, expected: AcceptingPatch")
            }

            val project = gitLabApi.getProject(projectModel.projectPath)
            val filePatch = patchModel.repositoryFilePatch
            val regex = filePatch.regex.ifBlank { throw IllegalArgumentException("Regex cannot be blank") }.toRegex()

            val mr = gitLabApi.getMergeRequest(project, currentState.mergeRequest.IID)
            val file = gitLabApi.getFile(project, filePatch.filePath, mr.sourceBranch)

            val processResult = processStringContent(file.decodedContentAsString, regex, filePatch.replacement)

            if (processResult.modified) {
                file.encodeAndSetContent(processResult.content)
                gitLabApi.updateFile(project, file, mr.sourceBranch, patchModel.commitMessage)
            }

            return AcceptedPatch
        }

        private fun processStringContent(initialContent: String, regex: Regex, replacement: String): ProcessStringResult {
            val matchResult = regex.find(initialContent)

            val range = when (matchResult?.groups?.size) {
                null -> IntRange.EMPTY
                1 -> matchResult.range
                else -> matchResult.groups[1]?.range ?: IntRange.EMPTY
            }

            val result = if (!range.isEmpty()) {
                initialContent.replaceRange(range, replacement)
            } else {
                println("Regex $regex didn't match anything in source content: \n $initialContent")
                initialContent
            }
            return ProcessStringResult(result, !range.isEmpty())
        }

        private data class ProcessStringResult(val content: String, val modified: Boolean)
    }

    class ResolveMergeRequest : WorkflowAction() {
        override fun execute(currentState: WorkflowState): WorkflowState {
            println("executed")
            return Finished
        }
    }
}
