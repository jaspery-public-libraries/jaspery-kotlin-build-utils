/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.workflow

data class GitLabPatchModel(val commitMessage: String, val repositoryFilePatch: RepositoryFilePatchModel) {
    data class RepositoryFilePatchModel(val filePath: String, val regex: String, val replacement: String)
}