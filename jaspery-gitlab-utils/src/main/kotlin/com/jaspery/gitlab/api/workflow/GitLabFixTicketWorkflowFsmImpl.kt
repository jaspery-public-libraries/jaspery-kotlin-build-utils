/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.workflow

import com.jaspery.gitlab.api.facade.GitLabApiFacade
import com.jaspery.gitlab.api.workflow.WorkflowAction.CreateMergeRequest
import com.jaspery.gitlab.api.workflow.WorkflowAction.ResolveMergeRequest
import com.jaspery.gitlab.api.workflow.WorkflowState.AcceptingPatch
import com.jaspery.gitlab.api.workflow.WorkflowState.Initialized

class GitLabFixTicketWorkflowFsmImpl(
        private val gitLabApiFacade: GitLabApiFacade,
        private val model: GitLabFixTicketWorkflowModel
) : GitLabFixTicketWorkflow {
    private var state: WorkflowState = Initialized

    override val mergeRequest get() = (state as? AcceptingPatch)?.mergeRequest ?: throw IllegalStateException()

    override fun createMergeRequest() {
        val action = CreateMergeRequest(gitLabApiFacade, model.projectModel, model.issueModel)
        state = state.consumeAction(action)
    }

    override fun submitPatch(init: GitLabPatchModelBuilder.() -> Unit) {
        val patchModel = GitLabPatchModelBuilder().apply { init() }.build()
        val action = WorkflowAction.SubmitPatch(gitLabApiFacade, model.projectModel, patchModel)
        state = state.consumeAction(action)
    }

    override fun resolveMergeRequest() {
        state = state.consumeAction(ResolveMergeRequest())
    }
}
