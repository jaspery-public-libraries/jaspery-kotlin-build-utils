/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.facade

import com.jaspery.gitlab.api.kotlin.lang.requireNotBlank
import org.gitlab4j.api.*
import org.gitlab4j.api.Constants.IssueState.OPENED
import org.gitlab4j.api.models.*

class GitLabApiFacade(
        private val groupApi: GroupApi,
        private val projectApi: ProjectApi,
        private val commitsApi: CommitsApi,
        private val repositoryApi: RepositoryApi,
        private val repositoryFileApi: RepositoryFileApi,
        private val mergeRequestApi: MergeRequestApi,
        private val issuesApi: IssuesApi
) {

    fun getOrCreateGroup(path: String, group: Group): Group {
        requireNotBlank(path) { "Path should not be blank" }
        require(group.path == path) { "Group path should be $path" }
        return groupApi.getGroup(path) ?: groupApi.addGroup(group)
    }

    fun getOrCreateProject(path: String, project: Project): Project {
        requireNotBlank(path) { "Path should not be blank" }
        return projectApi.getProject(path) ?: projectApi.createProject(project)
    }

    fun getProject(path: String): Project {
        requireNotBlank(path) { "Path should not be blank" }
        return projectApi.getProject(path) ?: throw IllegalArgumentException("Project $path not found")
    }

    fun createIssue(project: Project, title: String, description: String): Issue {
        requireNotBlank(title) { "Issue title should not be blank" }
        return issuesApi.createIssue(project, title, description)
    }

    fun getMergeRequest(project: Project, mergeRequestIID: Int): MergeRequest =
            mergeRequestApi.getMergeRequest(project, mergeRequestIID)

    fun getFile(project: Project, filePath: String, sourceBranch: String): RepositoryFile {
        requireNotBlank(filePath) { "File path should not be blank" }
        requireNotBlank(sourceBranch) { "Source branch should not be blank" }
        return repositoryFileApi.getFile(project, filePath, sourceBranch, true)
    }

    fun updateFile(project: Project, file: RepositoryFile, sourceBranch: String, commitMessage: String): RepositoryFile {
        requireNotBlank(sourceBranch) { "Source branch should not be blank" }
        requireNotBlank(commitMessage) { "Commit message should not be blank" }
        return repositoryFileApi.updateFile(project, file, sourceBranch, commitMessage)
    }

    fun createMultipleFiles(project: Project, branch: String, commitMessage: String, files: Collection<RepositoryFileContent>) {
        val actions = files
                .filter { !fileExists(project, it.path, branch) }
                .map { it.toCommitAction() }

        if (actions.isNotEmpty()) {
            val payload = CommitPayload().apply {
                this.branch = branch
                this.commitMessage = commitMessage
                this.actions = actions
            }

            commitsApi.createCommit(project, payload)
        }
    }

    fun createBranchAndMergeRequest(project: Project, newBranch: String, defaultBranch: String, title: String, description: String): MergeRequest {
        requireNotBlank(newBranch)
        requireNotBlank(defaultBranch)
        requireNotBlank(title)
        val b = repositoryApi.createBranch(project, newBranch, defaultBranch)
        return mergeRequestApi.createMergeRequest(project, b.name, defaultBranch, title, description, 0)
    }

    fun dropBranchAndMergeRequestIfOpen(project: Project, mrTitle: String) {
        requireNotBlank(mrTitle) { "Invalid merge request title: blank" }

        val filter = MergeRequestFilter().apply {
            this.projectId = project.id
            this.state = Constants.MergeRequestState.OPENED
        }

        val mrs = mergeRequestApi.getMergeRequests(filter).filter { it.title == mrTitle }

        mrs.forEach { mr ->
            mergeRequestApi.deleteMergeRequest(project, mr.iid)
            repositoryApi.deleteBranch(project, mr.sourceBranch)
        }
    }

    fun dropIssueIfOpen(project: Project, issueTitle: String) {
        requireNotBlank(issueTitle) { "Invalid issue title: blank" }

        val issues = issuesApi.getIssues(project, IssueFilter().withState(OPENED)).filter { it.title == issueTitle }

        issues.forEach { issuesApi.deleteIssue(project, it.iid) }
    }

    private fun fileExists(project: Project, path: String, branch: String) = repositoryFileApi.getOptionalFile(project, path, branch).isPresent

    private fun RepositoryFileContent.toCommitAction(): CommitAction = CommitAction().also {
        it.action = CommitAction.Action.CREATE
        it.filePath = this.path
        it.encoding = Constants.Encoding.TEXT
        it.content = this.content
    }

    internal class Builder(private val gitLabApi: GitLabApi) {
        fun build() = GitLabApiFacade(
                gitLabApi.groupApi,
                gitLabApi.projectApi,
                gitLabApi.commitsApi,
                gitLabApi.repositoryApi,
                gitLabApi.repositoryFileApi,
                gitLabApi.mergeRequestApi,
                gitLabApi.issuesApi
        )
    }

    class Factory(private val gitLabConnection: GitLabConnection) {
        fun newInstance() = Builder(with(gitLabConnection) { GitLabApi(hostUrl, privateToken) }).build()
    }
}

