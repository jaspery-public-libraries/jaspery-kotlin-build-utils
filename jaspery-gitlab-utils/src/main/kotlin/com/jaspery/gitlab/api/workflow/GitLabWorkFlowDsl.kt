/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api.workflow

import com.jaspery.gitlab.api.facade.GitLabApiFacade
import com.jaspery.gitlab.api.facade.GitLabConnection
import com.jaspery.gitlab.api.workflow.GitLabFixTicketWorkflowModel.IssueModel
import com.jaspery.gitlab.api.workflow.GitLabFixTicketWorkflowModel.ProjectModel
import com.jaspery.gitlab.api.workflow.GitLabPatchModel.RepositoryFilePatchModel

fun workflow(init: GitLabFixTicketWorkflowBuilder.() -> Unit) = GitLabFixTicketWorkflowBuilder().apply { init() }.build()

@GitLabWorkflowMarker
open class AbstractWorkflowDslElement

class GitLabFixTicketWorkflowBuilder : AbstractWorkflowDslElement() {
    private var connectionBuilder: ConnectionBuilder? = null
    private var projectBuilder: ProjectBuilder? = null
    private var issueBuilder: IssueBuilder? = null

    fun connection(init: ConnectionBuilder.() -> Unit) = ConnectionBuilder().apply { init() }.also { connectionBuilder = it }
    fun project(init: ProjectBuilder.() -> Unit): ProjectBuilder = ProjectBuilder().apply { init() }.also { projectBuilder = it }
    fun issue(init: IssueBuilder.() -> Unit): IssueBuilder = IssueBuilder().apply { init() }.also { issueBuilder = it }

    fun build(): GitLabFixTicketWorkflow = GitLabFixTicketWorkflowFsmImpl(
            GitLabApiFacade.Factory(checkNotNull(connectionBuilder).build()).newInstance(),
            GitLabFixTicketWorkflowModel(checkNotNull(projectBuilder).build(), checkNotNull(issueBuilder).build())
    )
}

class ConnectionBuilder(var hostUrl: String = "", var privateToken: String = "") : AbstractWorkflowDslElement() {
    fun build(): GitLabConnection = GitLabConnection(hostUrl, privateToken)
}

class ProjectBuilder(var groupId: String = "", var projectId: String = "") : AbstractWorkflowDslElement() {
    fun build() = ProjectModel(groupId, projectId)
}

class IssueBuilder(var title: String = "", var description: String = "") : AbstractWorkflowDslElement() {
    fun build() = IssueModel(title, description)
}

class RepositoryFilePatchBuilder(var filePath: String = "", var regex: String = "", var replacement: String = "") : AbstractWorkflowDslElement() {
    fun build() = RepositoryFilePatchModel(filePath, regex, replacement)
}

class GitLabPatchModelBuilder(var commitMessage: String = "") : AbstractWorkflowDslElement() {
    private var repositoryFilePatchBuilder: RepositoryFilePatchBuilder? = null
    fun file(init: RepositoryFilePatchBuilder.() -> Unit) = RepositoryFilePatchBuilder().apply { init() }.also {
        repositoryFilePatchBuilder = it
    }

    fun build() = GitLabPatchModel(commitMessage, checkNotNull(repositoryFilePatchBuilder).build())
}
