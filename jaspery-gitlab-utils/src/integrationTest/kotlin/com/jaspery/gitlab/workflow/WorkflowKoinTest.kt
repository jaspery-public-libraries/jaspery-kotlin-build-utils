/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.workflow

import com.jaspery.gitlab.api.facade.GitLabApiFacade
import com.jaspery.gitlab.api.facade.GitLabConnection
import com.jaspery.gitlab.api.workflow.workflow
import org.gitlab4j.api.models.Group
import org.gitlab4j.api.models.Project
import org.gitlab4j.api.models.Visibility
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import org.testng.annotations.AfterClass
import org.testng.annotations.BeforeClass
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

class WorkflowKoinTest : KoinTest {
    private val gitLabConnection: GitLabConnection by inject()
    private val gitLabApi: GitLabApiFacade by inject()

    //state
    private var group: Group = Group().apply {
        name = "jgl-it"
        path = "jgl-it"
        description = "jgl-it"
        visibility = Visibility.PRIVATE
    }

    private var project: Project = Project().withName("jgl-it-prj")

    @BeforeClass
    fun startKoinBeforeClass() {
        startKoin {
            modules(
                    module {
                        single { GitLabCredentialsDataProvider.GitLabCredentials.toConnection() }
                        single { GitLabApiFacade.Factory(get()).newInstance()}
                    }
            )
        }
    }

    @BeforeMethod//(dependsOnMethods = ["startKoinBeforeClass"])
    fun setUpGlGroup() {
        group = gitLabApi.getOrCreateGroup(group.path, group)
    }

    @BeforeMethod(dependsOnMethods = ["setUpGlGroup"])
    fun setUpGlProject() {
        project = gitLabApi.getOrCreateProject("${group.name}/${project.name}", project.withNamespaceId(group.id))
    }


    @AfterClass
    fun stopKoinAfterClass() {

    }

    @Test
    fun testSomething() {
        val workflow = workflow {
            connection {
                hostUrl = gitLabConnection.hostUrl
                privateToken = gitLabConnection.privateToken
            }
            project {
                groupId = group.path
                projectId = project.path
            }
            issue {
                title = "Upgrade Gradle"
                description = "test"
            }
        }
        workflow.createMergeRequest()
    }
}