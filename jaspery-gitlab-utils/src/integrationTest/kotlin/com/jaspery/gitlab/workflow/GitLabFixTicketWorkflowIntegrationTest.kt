/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.workflow

import com.jaspery.gitlab.api.facade.GitLabApiFacade
import com.jaspery.gitlab.api.facade.GitLabConnection
import com.jaspery.gitlab.api.facade.RepositoryFileContent
import com.jaspery.gitlab.api.workflow.workflow
import org.gitlab4j.api.models.Group
import org.gitlab4j.api.models.Project
import org.gitlab4j.api.models.Visibility
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import org.testng.ITestResult
import org.testng.annotations.*

class GitLabFixTicketWorkflowIntegrationTest: KoinTest {
    private val gitLabConnection: GitLabConnection by inject()
    private val gitLabApi: GitLabApiFacade by inject()

    @BeforeClass
    fun startKoinBeforeClass() {
        startKoin {
            modules(
                    module {
                        single { GitLabCredentialsDataProvider.GitLabCredentials.toConnection() }
                        single { GitLabApiFacade.Factory(get()).newInstance()}
                    }
            )
        }
    }

    @AfterClass
    fun stopKoinAfterClass() {
        stopKoin()
    }

    //state
    private var group: Group = Group().apply {
        name = "jgl-it"
        path = "jgl-it"
        description = "jgl-it"
        visibility = Visibility.PRIVATE
    }

    private var project: Project = Project().withName("jgl-it-prj")

    // state
    private var mergeRequestIID: Int = 0

    @BeforeMethod
    fun setUpGlGroup() {
        group = gitLabApi.getOrCreateGroup(group.path, group)
    }

    @BeforeMethod(dependsOnMethods = ["setUpGlGroup"])
    fun setUpGlProject() {
        project = gitLabApi.getOrCreateProject("${group.name}/${project.name}", project.withNamespaceId(group.id))
    }

    @BeforeMethod(dependsOnMethods = ["setUpGlProject"])
    fun setUpGlRepository() {
        gitLabApi.createMultipleFiles(project, BranchName, CommitMessage, DefaultRepositoryFiles)
    }

    @Test
    fun test() {
        val workflow = workflow {
            connection {
                hostUrl = gitLabConnection.hostUrl
                privateToken = gitLabConnection.privateToken
            }
            project {
                groupId = group.path
                projectId = project.path
            }
            issue {
                title = "Upgrade Gradle"
                description = "test"
            }
        }
        workflow.createMergeRequest()
        mergeRequestIID = workflow.mergeRequest.IID
        workflow.submitPatch {
            commitMessage = "asdf"
            file {
                filePath = "gradle.properties"
                regex = "kotlinVersion=(1\\.3\\.50)"
                replacement = "1.3.61"
            }
        }
        workflow.resolveMergeRequest()
    }

    @AfterMethod
    fun dropBranchAndMergeRequest(result: ITestResult) {
        if (!result.isSuccess) {
            gitLabApi.dropBranchAndMergeRequestIfOpen(project, "Upgrade Gradle")
            gitLabApi.dropIssueIfOpen(project, "Upgrade Gradle")
        }
    }

    companion object TestData {
        const val BranchName = "master"
        const val CommitMessage = "[test data] Initial Repo SetUp"
        val DefaultRepositoryFiles = listOf(
                RepositoryFileContent("README.md",
                        """
                            This is default README file for test repo
                            And this is new line
                        """.trimIndent()),
                RepositoryFileContent("gradle/wrapper/gradle-wrapper.properties",
                        """
                            distributionBase=GRADLE_USER_HOME
                            distributionPath=wrapper/dists
                            distributionUrl=https\://services.gradle.org/distributions/gradle-5.6.3-all.zip
                            zipStoreBase=GRADLE_USER_HOME
                            zipStorePath=wrapper/dists
                        """.trimIndent()),
                RepositoryFileContent("gradle.properties",
                        """
                            kotlinVersion=1.3.50
                            springDepMgtVersion=1.0.8.RELEASE
                        """.trimIndent())
        )
    }
}