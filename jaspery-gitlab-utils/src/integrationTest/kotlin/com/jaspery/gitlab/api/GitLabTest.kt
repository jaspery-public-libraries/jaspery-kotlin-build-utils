package com.jaspery.gitlab.api

import assertk.all
import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNotNull
import assertk.assertions.prop
import org.gitlab4j.api.Constants
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.GitLabApiException
import org.gitlab4j.api.models.Group
import org.gitlab4j.api.models.Issue
import org.gitlab4j.api.models.IssueFilter
import org.gitlab4j.api.models.Project
import org.testng.annotations.AfterTest
import org.testng.annotations.Test
import java.lang.Math.random
import kotlin.test.assertFailsWith

object JasperyPublicLibraries {
    const val id = "jaspery-public-libraries"

    object JasperyKotlinExt {
        const val id = "jaspery-kotlin-ext"
        val path = "${JasperyPublicLibraries.id}/$id"
    }
}

object IssueForTestSpec {
    val title = "test ${random()}"
    val description = "test description ${random()}"
}

@Test
class GitLabTest {
    private val gitLabApi = GitLabApi(GitLabCredentials.server, GitLabCredentials.apiToken)

    private val groupApi = gitLabApi.groupApi
    private val projectApi = gitLabApi.projectApi
    private val issuesApi = gitLabApi.issuesApi
    private val mergeRequestApi = gitLabApi.mergeRequestApi
    private val repositoryApi = gitLabApi.repositoryApi

    private lateinit var group: Group
    private lateinit var project: Project
    private lateinit var createdIssue: Issue

    @Test
    fun `test get GitLab group which does not exist`() {
        assertFailsWith<GitLabApiException> {
            groupApi.getGroup(JasperyPublicLibraries.id + "1234" + random())
        }
    }

    @Test
    fun `test get GitLab group`() {
        group = groupApi.getGroup(JasperyPublicLibraries.id)
        println(group)
        assertThat(group, "group").all {
            isNotNull()
            prop("name") { it.name }.isEqualTo(JasperyPublicLibraries.id)
        }
    }

    @Test
    fun `test get GitLab project`() {
        project = projectApi.getProject(JasperyPublicLibraries.JasperyKotlinExt.path)
        assertThat(project, "project").all {
            prop("name") { it.name }.isEqualTo("jaspery-kotlin-ext")
        }
    }

    @Test(dependsOnMethods = ["test get GitLab project"])
    fun `test create issue with GitLab project`() {
        createdIssue = issuesApi.createIssue(project, IssueForTestSpec.title, IssueForTestSpec.description)
        assertThat(createdIssue, "createdIssue").all {
            prop("title") { it.title }.isEqualTo(IssueForTestSpec.title)
            prop("description") { it.description }.isEqualTo(IssueForTestSpec.description)
        }
    }

    @Test(dependsOnMethods = ["test get GitLab project", "test create issue with GitLab project"])
    fun `test retrieve created issue`() {
        val issues = issuesApi.getIssues(project, IssueFilter().withSearch("test").withState(Constants.IssueState.OPENED))
        val retrievedIssue = issues.first()
        assertThat(retrievedIssue, "retrievedIssue").all {
            prop("title") { it.title }.isEqualTo(IssueForTestSpec.title)
            prop("description") { it.description }.isEqualTo(IssueForTestSpec.description)
        }
    }

    @Test(dependsOnMethods = ["test get GitLab project", "test create issue with GitLab project"])
    fun `test create merge request`() {
//        repositoryApi.get
        val i = createdIssue
        val defaultBranch = project.defaultBranch
        val newBranch = i.title.replace(Regex("\\s+"), "-")
        val b = repositoryApi.createBranch(project, newBranch, defaultBranch)
        val mr = mergeRequestApi.createMergeRequest(project, b.name, defaultBranch, i.title, i.description, 0)
    }

    @AfterTest
    fun cleanUpIssueAndMergeRequest() {
        issuesApi.deleteIssue(project, createdIssue.iid)
    }
}
