/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gitlab.api

import java.util.*

object GitLabCredentials {
    private const val RESOURCE = "credentials-gitlab.properties"
    private val properties by lazy { readGitLabCredentials() }

    val server: String get() = properties.getProperty("gitlab.server")
    val apiToken: String get() = properties.getProperty("gitlab.apiToken")

    private fun readGitLabCredentials() = javaClass.classLoader
            .getResourceAsStream(RESOURCE).use {
                Properties().apply { load(it) }
            }
}