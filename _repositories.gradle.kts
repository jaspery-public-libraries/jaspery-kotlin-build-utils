/*
 * Copyright © 2021. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

@Suppress("UnstableApiUsage")
fun MavenRepositoryContentDescriptor.approvedContentJetBrains() {
    includeModule("org.jetbrains", "annotations")
    includeModule("org.jetbrains.intellij.deps", "trove4j")
    // JetBrains Kotlin
    includeModuleByRegex("org\\.jetbrains\\.kotlin", "kotlin-.*")
    includeModuleByRegex("org\\.jetbrains\\.kotlinx", "kotlinx-coroutines-core")

    // Other transitive
    includeModule("de.undercouch", "gradle-download-task")
    includeModuleByRegex("com\\.google\\.code\\.gson", "gson(-parent)*")
    includeModule("org.sonatype.oss", "oss-parent")
    // For IDE only?
    includeModule("com.github.gundy", "semver4j")
    includeModuleByRegex("org\\.antlr", "antlr4-(runtime|master)")
}

@Suppress("UnstableApiUsage")
fun MavenRepositoryContentDescriptor.approvedContentAssertKJvm() {
    includeModuleByRegex("com\\.willowtreeapps\\.assertk", "assertk-(jvm|metadata)")
    includeModuleByRegex("com\\.willowtreeapps\\.opentest4k", "opentest4k(-jvm)*")
    includeModule("org.opentest4j", "opentest4j")
}

@Suppress("UnstableApiUsage")
fun MavenRepositoryContentDescriptor.approvedContentKoin() {
    includeModuleByRegex("io\\.insert-koin", "koin-(core|test|test-core|core-jvm|test-jvm)")
}

@Suppress("UnstableApiUsage")
fun MavenRepositoryContentDescriptor.approvedContentMockk() {
    includeModuleByRegex("io\\.mockk", "mockk(-common|-dsl|-jvm|-agent(-common|-jvm|-api))*")
    includeModuleByRegex("org\\.objenesis", "objenesis(-parent)*")
    includeModuleByRegex("net\\.bytebuddy", "byte-buddy(-agent|-parent)*")
}

@Suppress("UnstableApiUsage")
fun MavenRepositoryContentDescriptor.approvedContentTestng() {
    includeModule("org.testng", "testng")
    includeModule("com.beust", "jcommander")
    includeModuleByRegex("org\\.apache\\.ant", "ant(-launcher|-parent)*")
    includeModule("com.google", "google")
    includeGroupByRegex("com\\.google\\.(inject|guava|code\\.findbugs|errorprone|j2objc)")
    includeModule("org.yaml", "snakeyaml")
    includeModule("javax.inject", "javax.inject")
    includeModule("aopalliance", "aopalliance")
    includeModule("org.checkerframework", "checker-compat-qual")
    includeModule("org.codehaus", "codehaus-parent")
    includeModuleByRegex("org\\.codehaus\\.mojo", "(mojo-parent|animal-sniffer-(annotations|parent))")
    includeModule("org.webjars", "jquery")
}

@Suppress("UnstableApiUsage")
fun MavenRepositoryContentDescriptor.approvedContentGitLabApi() {
    // gitlab api
    includeModule("org.gitlab4j", "gitlab4j-api")
    includeModule("com.sun.activation", "all")
    includeModule("org.eclipse.ee4j", "project")
    includeModule("org.javassist", "javassist")
    includeModule("org.jvnet.mimepull", "mimepull")

    includeGroupByRegex("jakarta\\.(activation|annotation|servlet|ws\\.rs.*|xml\\.bind)")

    includeModule("com.fasterxml", "oss-parent")
    includeGroupByRegex("com\\.fasterxml\\.jackson(\\.(module|core|jaxrs))*")

    includeGroupByRegex("org\\.glassfish\\.jersey(\\.(connectors|core|inject|media))*")
    includeModuleByRegex("org\\.glassfish\\.hk2", "(external|hk2-.+|osgi-resource-locator)")
    includeModuleByRegex("org\\.glassfish\\.hk2\\.external", "(jakarta\\.inject|aopalliance-repackaged)")

    includeModule("org.apache.httpcomponents", "httpclient")
    includeModuleByRegex("org\\.apache\\.httpcomponents", "(httpcomponents-(client|parent|core)|httpcore)")
    includeModule("org.apache", "apache")
    includeModule("org.apache.commons", "commons-parent")
    includeModule("commons-codec", "commons-codec")
    includeModule("commons-logging", "commons-logging")
}

repositories {
    mavenCentral {
        @Suppress("UnstableApiUsage")
        mavenContent {
            approvedContentJetBrains()

            approvedContentAssertKJvm()
            approvedContentMockk()
            approvedContentKoin()
            approvedContentTestng()
            approvedContentGitLabApi()
        }
    }

    maven("https://gitlab.com/api/v4/projects/18935877/packages/maven") {
        name = "GitLab::m2-repo::JPL"
        mavenContent {
            includeModule("com.jaspery.jaspery-public-libraries", "jpl-dependencies-platform")
        }
    }

    maven("https://jitpack.io") {
        @Suppress("UnstableApiUsage")
        mavenContent {
            includeModule("com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext", "kotlin-testng-dataprovider")
        }
    }
}
