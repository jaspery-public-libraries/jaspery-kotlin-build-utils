/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */
plugins {
    `kotlin-dsl-base`
}

dependencies {
    implementation(gradleApi())
}
