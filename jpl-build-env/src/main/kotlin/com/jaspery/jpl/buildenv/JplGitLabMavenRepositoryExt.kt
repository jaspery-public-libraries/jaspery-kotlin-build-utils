/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.jpl.buildenv

import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.artifacts.repositories.MavenArtifactRepository

@Suppress("unused")
fun RepositoryHandler.jplGitLabMavenRepo(action: MavenArtifactRepository.() -> Unit) =
        gitLabMavenRepo(JPL_GL_M2_REPO_PRJ_ID, "GitLab::m2-repo::JPL") {
            addJplApprovedContent()
            action(this)
        }

@Suppress("unused")
fun RepositoryHandler.jplGitLabMavenRepo() = jplGitLabMavenRepo { }

private fun MavenArtifactRepository.addJplApprovedContent() = mavenContent {
    includeModule("com.jaspery.jaspery-public-libraries", "jpl-dependencies-platform")
}

fun RepositoryHandler.gitLabMavenRepo(projectId: String, repoName: String, action: MavenArtifactRepository.() -> Unit) =
        maven {
            setUrl("https://gitlab.com/api/v4/projects/$projectId/packages/maven")
            name = repoName
            action(this)
        }
